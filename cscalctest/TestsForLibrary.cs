﻿using baselib;
using cscalclib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalctest
{
	[TestFixture]
	public class TestsForLibrary : TestBase
	{
		[Test]
		public void LoadInputData()
		{
			for (int i = 0; i < Manager.GradesNames.Length; i++)
			{
				Console.WriteLine("{{{0}}} {1}", Manager.GradesNames[i], Manager.Samples[i]);
			}
		}

		[Test]
		public void Shuffle()
		{
			// TODO: Check that shuffle algorithm works here
		}

		[Test]
		public void ExecMain()
		{
			Manager.GenerateReports();
		}
	}
}
