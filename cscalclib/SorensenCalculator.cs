﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	class SorensenCalculator : CalculatorBase
	{
		public SorensenCalculator(Sample[] samples, Grade[] grades)
			: base(samples, grades)
		{
		}

		protected override decimal ComparisonFactor(Sample sampleOne, Sample sampleTwo)
		{
			int SpeciesNumberInSampleOneOnly = 0;
			int SpeciesNumberInSampleTwoOnly = 0;
			int SpeciesNumberInBothSamples = 0;

			for (uint i = 0; i < sampleOne.SpeciesQuantitiesNumber; i++)
			{
				if ((sampleOne[i] > 0) && (sampleTwo[i] > 0))
				{
					SpeciesNumberInBothSamples++;
				}
				else if (sampleOne[i] > 0)
				{
					SpeciesNumberInSampleOneOnly++;
				}
				else if (sampleTwo[i] > 0)
				{
					SpeciesNumberInSampleTwoOnly++;
				}
			}

			decimal numerator = (2.0m * SpeciesNumberInBothSamples);
			decimal denominator = numerator + SpeciesNumberInSampleOneOnly + SpeciesNumberInSampleTwoOnly;

			return numerator / denominator;
		}
	}
}
