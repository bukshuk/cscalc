﻿using interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	internal class Shuffler
	{
		public Shuffler(Sample[] samples, string[] gradesNames, uint numberOfUniqueShuffles = 0)
		{
			Samples = samples;
			GradesNames = gradesNames.ToArray();
			GradesNamesPool = new List<string[]>();

			if (numberOfUniqueShuffles == 0)
			{
				NumberOfUniqueShuffles = Settings.Default.NumberOfUniqueShuffles;
			}
		}

		internal void Shuffle()
		{
			GradesNamesPool.Clear();
			
			long count = 0;

			while (GradesNamesPool.Count < NumberOfUniqueShuffles)
			{
				Shuffler.ShuffleGrades(GradesNames);
				if (GradesNamesPool.FirstOrDefault(g => g.SequenceEqual(GradesNames)) == null)
				{
					GradesNamesPool.Add(GradesNames.ToArray());
					OutputCounter(++count, 500, "Shuffled: {0}");
				}
			}

			Console.WriteLine("Done.");
			Console.WriteLine();
		}

		internal void Calculate(decimal chekanovskyM, decimal sorensenM)
		{
			ChekanovskyMs = new List<decimal>();
			SorensenMs = new List<decimal>();

			long count = 0;

			foreach (var gradesNames in GradesNamesPool)
			{
				var estimator = new Estimator(Samples, gradesNames);
				var result = estimator.Calculate();

				OutputCounter(++count, 50, "Calculated: {0}");

				ChekanovskyMs.Add(result.Item1);
				SorensenMs.Add(result.Item2);
			}

			Console.WriteLine();
			Console.WriteLine("Sorensen Ms disctinct: {0}", SorensenMs.Distinct().Count());
			Console.WriteLine("Chekanovsky Ms disctinct: {0}", ChekanovskyMs.Distinct().Count());

			SorensenLessMore = LessMore(sorensenM, SorensenMs);
			ChekanovskyLessMore = LessMore(chekanovskyM, ChekanovskyMs);
		}

		private static void OutputCounter(long counter, long divider, string format)
		{
			long remainder;
			Math.DivRem(counter, divider, out remainder);

			if (remainder == 0)
			{
				Console.WriteLine(format, counter);
			}
		}

		private static Tuple<uint, uint, uint> LessMore(decimal m, List<decimal> ms)
		{
			uint less = 0;
			uint more = 0;
			uint equal = 0;

			foreach (var item in ms)
			{
				if (item > m)
				{
					more++;
				}
				if (item < m)
				{
					less++;
				}
				if (item == m)
				{
					equal++;
				}
			}

			return Tuple.Create(less, equal, more);
		}

		private static void ShuffleGrades(string[] gradesNames)
		{
			Random randomManager = new Random();

			int size = gradesNames.Length;
			for (int i = 0; i < size - 1; i++)
			{
				int randomIndex = i;
				while (randomIndex == i)
				{
					randomIndex = randomManager.Next(i, size);
				}

				Shuffler.Swap(gradesNames, i, randomIndex);
			}
		}

		private static void Swap(string[] gradesNames, int i, int j)
		{
			string tempString = gradesNames[i];
			gradesNames[i] = gradesNames[j];
			gradesNames[j] = tempString;
		}


		private Sample[] Samples { get; set; }

		private string[] GradesNames { get; set; }

		private uint NumberOfUniqueShuffles { get; set; }

		private List<string[]> GradesNamesPool { get; set; }

		private List<decimal> SorensenMs { get; set; }

		private List<decimal> ChekanovskyMs { get; set; }

		internal Tuple<uint, uint, uint> SorensenLessMore { get; set; }
		
		internal Tuple<uint, uint, uint> ChekanovskyLessMore { get; set; }
	}
}
