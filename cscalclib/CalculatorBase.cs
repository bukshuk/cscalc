﻿using interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	abstract class CalculatorBase
	{
		public CalculatorBase(Sample[] samples, Grade[] grades)
		{
			this.Samples = samples;
			this.Grades = grades;
		}

		internal decimal Calculate()
		{
			ComparisonMatrix = GetComparisonMatrix(Samples);

			MeanValuesMatrix = CalculatorBase.GetMeanValuesMatix(ComparisonMatrix, Grades);

			BNumeratorMatrix = CalculatorBase.GetBNumeratorMatrix(ComparisonMatrix, Grades);
			BDenominatorMatrix = CalculatorBase.GetBDenominatorMatrix(Grades);

			BNumerator = CalculatorBase.GetMatrixSum(BNumeratorMatrix);
			BDenominator = CalculatorBase.GetMatrixSum(BDenominatorMatrix);

			BMean = BNumerator / BDenominator;
			WMean = CalculatorBase.GetWMean(MeanValuesMatrix, Grades);

			M = BMean / WMean;

			return M;
		}


		private static decimal[,] GetBNumeratorMatrix(decimal[,] matrix, Grade[] grades)
		{
			return GetBCommonMatrix(matrix, grades,
				(gradeOne, gradeTwo, mx) => MeanValue(gradeOne, gradeTwo, mx) * gradeOne.Indexes.Length * gradeTwo.Indexes.Length);
		}

		private static decimal[,] GetBDenominatorMatrix(Grade[] grades)
		{
			return GetBCommonMatrix(null, grades, (gradeOne, gradeTwo, m) => gradeOne.Indexes.Length * gradeTwo.Indexes.Length);
		}

		private static decimal[,] GetBCommonMatrix(decimal[,] matrix, Grade[] grades, Func<Grade, Grade, decimal[,], decimal> bFunc)
		{
			var returnArray = new decimal[grades.Length, grades.Length];

			for (int i = 0; i < grades.Length; i++)
			{
				for (int j = 0; j < grades.Length; j++)
				{
					returnArray[i, j] = (j < i) ? bFunc(grades[i], grades[j], matrix) : CalculatorBase.EmptyMarker;
				}
			}

			return returnArray;
		}

		private static decimal GetMatrixSum(decimal[,] matrix)
		{
			Signaller.Exception(matrix.GetLength(0) == matrix.GetLength(1), "Should be square matrix");

			decimal sum = 0.0m;

			for (int i = 1; i < matrix.GetLength(0); i++)
			{
				for (int j = 0; j < i; j++)
				{
					sum += matrix[i, j];
				}
			}

			return sum;
		}

		private static decimal GetWMean(decimal[,] matrix, Grade[] grades)
		{
			Signaller.Exception(matrix.GetLength(0) == grades.Length, "The matrix should corespond to the grades number");
			Signaller.Exception(matrix.GetLength(0) == matrix.GetLength(1), "Should be square matrix");

			decimal sum = 0.0m;

			for (int i = 0; i < matrix.GetLength(0); i++)
			{
				sum += matrix[i, i] * grades[i].Indexes.Length;
			}

			return sum / grades.Sum(g => g.Indexes.Length);
		}

		private decimal[,] GetComparisonMatrix(Sample[] samples)
		{
			var returnMatrix = new decimal[samples.Length, samples.Length];

			for (int i = 0; i < samples.Length; i++)
			{
				for (int j = 0; j < samples.Length; j++)
				{
					returnMatrix[i, j] = (j < i) ? ComparisonFactor(samples[i], samples[j]) : CalculatorBase.EmptyMarker;
				}
			}

			return returnMatrix;
		}

		protected abstract decimal ComparisonFactor(Sample sampleOne, Sample sampleTwo);

		private static decimal[,] GetMeanValuesMatix(decimal[,] matrix, Grade[] grades)
		{
			var returnArray = new decimal[grades.Length, grades.Length];

			for (int i = 0; i < grades.Length; i++)
			{
				for (int j = 0; j < grades.Length; j++)
				{

					returnArray[i, j] = (j <= i) ? MeanValue(grades[i], grades[j], matrix) : CalculatorBase.EmptyMarker;
				}
			}

			return returnArray;
		}

		private static decimal MeanValue(Grade gradeOne, Grade gradeTwo, decimal[,] matrix)
		{
			decimal value = 0;
			int count = 0;

			foreach (var indexOne in gradeOne.Indexes)
			{
				foreach (var indexTwo in gradeTwo.Indexes)
				{
					if (indexOne != indexTwo)
					{
						value += matrix[Math.Max(indexOne, indexTwo), Math.Min(indexOne, indexTwo)];
						count++;
					}
				}
			}

			return value / count;
		}


		public decimal[,] ComparisonMatrix { get; private set; }

		public decimal[,] MeanValuesMatrix { get; private set; }

		public decimal[,] BNumeratorMatrix { get; private set; }

		public decimal[,] BDenominatorMatrix { get; private set; }

		public decimal BNumerator { get; private set; }

		public decimal BDenominator { get; private set; }

		public decimal BMean { get; private set; }

		public decimal WMean { get; private set; }

		public decimal M { get; private set; }


		public const decimal EmptyMarker = -1000.0m;


		private Sample[] Samples { get; set; }

		private Grade[] Grades { get; set; }
	}
}
