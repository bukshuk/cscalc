﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	class Grade
	{
		public string Name
		{
			get;
			set;
		}

		public int[] Indexes
		{
			get;
			set;
		}
	}
}
