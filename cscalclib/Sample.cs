﻿using interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	public class Sample : IEnumerable<decimal>
	{
		public Sample(string[] columns)
		{
			Signaller.Exception(columns.Length > 1, "The columns number must be greater than 1");

			_id = columns[0];
			_speciesQuantities = GetSpeciesQuantities(columns).ToArray();
		}

		public string Id
		{
			get
			{
				return _id;
			}
		}

		public int SpeciesQuantitiesNumber
		{
			get
			{
				return _speciesQuantities.Length;
			}
		}

		public decimal this[uint index]
		{
			get
			{
				Signaller.Exception(index < this.SpeciesQuantitiesNumber,
					"The index {0} must be less than {1} array size", index, this.SpeciesQuantitiesNumber);

				return _speciesQuantities[index];
			}
		}

		public override string ToString()
		{
			return string.Format("ID: {0} [{1}]", Id, string.Join("] [", _speciesQuantities));
		}

		public IEnumerator<decimal> GetEnumerator()
		{
			foreach (var item in _speciesQuantities)
			{
				yield return item;
			}
		}

		private IEnumerable<decimal> GetSpeciesQuantities(string[] columns)
		{
			for (int i = 1; i < columns.Length; i++)
			{
				decimal result;

				Signaller.Exception(decimal.TryParse(columns[i], out result), "{0} must be an decimal value", columns[i]);

				yield return result;
			}
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		private readonly string _id;

		private readonly decimal[] _speciesQuantities;
	}
}
