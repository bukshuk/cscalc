﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cscalclib
{
	internal class Estimator
	{
		internal Estimator(Sample[] samples, string[] gradesNames)
		{
			Grade[] grades = gradesNames
				.Select((n, i) => new
				{
					Name = n,
					Index = i
				})
				.GroupBy(c => c.Name)
				.OrderBy(g => g.Key)
				.Select(g => new Grade()
				{
					Name = g.Key,
					Indexes = g.Select(c => c.Index).ToArray()
				})
				.ToArray();

			ChekanovskyCalc = new ChekanovskyCalculator(samples, grades);
			SorensenCalc = new SorensenCalculator(samples, grades);
		}

		internal Tuple<decimal, decimal> Calculate()
		{
			return new Tuple<decimal, decimal>(ChekanovskyCalc.Calculate(), SorensenCalc.Calculate());
		}

		internal CalculatorBase ChekanovskyCalc
		{
			get;
			private set;
		}

		internal CalculatorBase SorensenCalc
		{
			get;
			private set;
		}
	}
}
